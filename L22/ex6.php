<?php
    if(isset($_FILES['image'])){
		move_uploaded_file($_FILES['image']['tmp_name'],"pic.jpg");
    }
?>
<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta charset="utf-8">
    <style>
    * {
        box-sizing: border-box;
    }
    
    table {
        margin: auto;
        border: 1px solid #2196F3;
        padding: 0;
        table-layout: fixed;
    }
    
    tr,
    td {
        border: 0;
        border-collapse: collapse;
        padding: 0;
        overflow: hidden;
        white-space: nowrap;
    }
    
    td {
        cursor: pointer;
        font: bold 50px 'Courier New';
        width: 100px;
        height: 100px;
        vertical-align: middle;
        text-align: center;
        transition: background .2s;
        position: relative;
    }
    
    img {
        margin: 0;
        position: absolute;
    }
    
    td:hover {
        background: #CCC;
    }
    
    td:before {
        content: "X";
        position: absolute;
        top: 20px;
        left: 35px
    }

    form {
    	border: 1px solid;
    	display: inline-block;
    	padding: 10px
    }
    </style>
</head>

<body onload="ldt()">
    <form action="">
    	<input type="button" value="Rezolvă automat" onclick="solveIt()">
    </form>
    <br>
    <form action="" method="POST" enctype="multipart/form-data">
    	Schimbă imaginea <input type="file" name="image">
    	<input type="submit">
    </form>
    <table border="1" id="gg">
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <script>
        var arr = [],
            p = [],
            empty, emptyK

        function ldt() {
            where = document.getElementById('gg')
            for (var i = 0, row; row = where.rows[i]; i++)
                for (var j = 0, col; col = row.cells[j]; j++)
                    p.push(i + '' + j)
            newGame()
        }

        function newPiece(xy, k) {
            x = parseInt(xy[0])
            y = parseInt(xy[1])
            var piece = document.createElement('img')
            piece.src = 'pic.jpg'
            piece.style.height = where.rows.length + '00%'
            piece.style.width = where.rows.length + '00%'
            piece.style.top = -(x * 100) + '%'
            piece.style.left = -(y * 100) + '%'
            piece.setAttribute('xy', x + '' + y)
            piece.setAttribute('k', k)
            return piece;
        }

        function newGame() {
            arr = []
            for (var i = 0, row; row = where.rows[i]; i++)
                for (var j = 0, col; col = row.cells[j]; j++)
                    if (i != 3 || j != 3)
                        arr.push(i + '' + j)
                    else
                        arr.push('-')
            var i = arr.length
            while (i--) {
                j = Math.floor(Math.random() * (i + 1))
                tmp = arr[i]
                arr[i] = arr[j]
                arr[j] = tmp
            }
            for (var i = 0, k = 0, row; row = where.rows[i]; i++) {
                for (var j = 0, col; col = row.cells[j]; j++) {
                    col.onclick = function() {
                        if (this.innerHTML == '') return
                        k = this.firstChild.getAttribute('k')
                        x = this.firstChild.getAttribute('xy')[0]
                        y = this.firstChild.getAttribute('xy')[1]
                        if (!canMove(p[k], empty)) return
                        arr[emptyK] = arr[k]
                        arr[k] = '-'
                        emptyK = k
                        empty = x + '' + y
                        showData()
                    }
                }
            }
            showData()
        }

        function showData() {
            for (var i = 0, k = 0, row; row = where.rows[i]; i++) {
                for (var j = 0, col; col = row.cells[j]; j++) {
                    col.innerHTML = ''
                    if (arr[k] != '-')
                        col.appendChild(newPiece(arr[k], k))
                    else {
                        empty = i + '' + j
                        emptyK = k
                    }
                    k++
                }
            }
            if (isSolved()) {
                again = prompt("Bravo! New Game? [yes/no]")
                if (again != null && again.toLowerCase() == 'yes')
                    newGame()
                else
                    alert("OK, bro")
            } else
                console.log('Nope!')
        }

        function canMove(fromXY, toXY) {
            fromX = parseInt(fromXY[0])
            fromY = parseInt(fromXY[1])
            toX = parseInt(toXY[0])
            toY = parseInt(toXY[1])
            return (toX + 1 == fromX && toY == fromY) || (toX - 1 == fromX && toY == fromY) || (toY + 1 == fromY && toX == fromX) || (toY - 1 == fromY && toX == fromX)
        }

        function isSolved() {
            s = []
            d = []
            for (var i = 0, k = 0, row; row = where.rows[i]; i++) {
                for (var j = 0, col; col = row.cells[j]; j++) {
                    d.push(i != 3 || j != 3 ? (i + '' + j) : '-')
                    s.push(col.innerHTML == '' ? '-' : col.firstChild.getAttribute('xy'))
                }
            }
            for (i = 0; i < d.length; i++)
                if (d[i] != s[i])
                    return false
            return true
        }

        function solveIt() {
            d = []
            for (var i = 0, row; row = where.rows[i]; i++)
                for (var j = 0, col; col = row.cells[j]; j++)
                    if (i != 3 || j != 3)
                        d.push(i + '' + j)
            d.push('-')
            arr = d
            showData()
        }
        </script>
</body>

</html>
