<html>

<head>
    <title></title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <form method="POST" action="data.php">
        <label for="fname">Nume</label>
        <br>
        <input id="fname" type="text" name="fname">
        <br>
        <label for="lname">Prenume</label>
        <br>
        <input id="lname" type="text" name="lname">
        <br>
        <label for="age">Varsta</label>
        <br>
        <input id="age" type="number" name="age" min="1" max="100">
        <br>
        <label for="email">Email</label>
        <br>
        <input id="email" type="email" name="email">
        <br>
        <label>Sex</label>
        <br>
        <label>Masculin</label>
        <input type="radio" name="sex" value="m">
        <label>Feminin</label>
        <input type="radio" name="sex" value="f">
        <br>
        <label>Limbi cunoscute</label>
        <br>
        <label>Engleza</label>
        <input type="checkbox" name="en">
        <label>Romana</label>
        <input type="checkbox" name="ro">
        <br>
        <br>
        <input class="b" type="submit" value="submit">
        <input class="b" type="reset" value="reset">
    </form>
</body>

</html>
