<?php
include("global.php");
$fname = "";
$lname = "";
$group = "";
$speciality = "";
$age = "";
$sex = "";
$address = "";
$username = "";
$password = "";
$editAge = "";
$editSex = "";
echo $_POST["fname"];
if($_SERVER["REQUEST_METHOD"] == "POST") {
    $fname = isset($_POST["fname"])?validateText($_POST["fname"]):"";
    $lname = isset($_POST["lname"])?validateText($_POST["lname"]):"";
    $group = isset($_POST["group"])?validateIsSomethig($_POST["group"]):"";
    $speciality = isset($_POST["speciality"])?validateText($_POST["speciality"]):"";
    $age = isset($_POST["age"])?validateAge($_POST["age"]):"";
    $sex = isset($_POST["sex"])?validateSex($_POST["sex"]):"-";
    $address = isset($_POST["address"])?validateIsSomethig($_POST["address"]):"";
    $username = isset($_POST["username"])?validateIsSomethig($_POST["username"]):"";
    $password = isset($_POST["password"])?validateIsSomethig($_POST["password"]):"";
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Magic</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <?php include("include/nav.php"); ?>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Înregistrare</h3>
                        </div>
                        <div class="panel-body">
                            <form action="validate.php" method="POST" role="form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="lname">Nume</label>
                                            <input type="text" class="form-control" id="lname" name="lname" value="<?php echo $lname; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="fname">Prenume</label>
                                            <input type="text" class="form-control" id="fname" name="fname" value="<?php echo $fname; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="group">Grupa</label>
                                            <input type="text" class="form-control" id="group" name="group" value="<?php echo $group; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="speciality">Specialitatea</label>
                                            <input type="text" class="form-control" id="speciality" name="speciality" value="<?php echo $speciality; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="age">Vârsta</label>
                                            <input type="text" class="form-control" id="age" name="age" value="<?php echo $age; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="sex">Sex</label>
                                            <select name="sex" id="sex" class="form-control">
                                                <option value="0">Alege</option>
                                                <option value="m">Masculin</option>
                                                <option value="f">Feminin</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="address">Adresa</label>
                                            <input type="text" class="form-control" id="address" name="address" value="<?php echo $address; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="username">Username</label>
                                            <input type="text" class="form-control" id="username" name="username" value="<?php echo $username; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="password">Parola</label>
                                            <input type="password" class="form-control" id="password" name="password">
                                        </div>
                                    </div>
                                </div>
                                <div class="btns pull-right">
                                    <button type="reset" class="btn btn-default" name="actionInsert">Reset</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
</body>

</html>
