<?php
include("global.php");
$isok = 1;
$fname = "";
$lname = "";
$group = "";
$speciality = "";
$age = "";
$sex = "";
$address = "";
$username = "";
$password = "";
$editAge = "";
$editSex = "";
$editFname = "";
$editLname = "";
$editGroup = "";
$editSpeciality = "";
$editAddress = "";
$editUsername = "";
if($_SERVER["REQUEST_METHOD"] == "POST") {
	if(!(isset($_POST["fname"]) && $fname = validateText($_POST["fname"]))) {
		$editFname = $fname;
		$fname = $error;
		$isok = 0;
	}
	else {
		$editFname = "";
	}
	if(!(isset($_POST["lname"]) && $lname = validateText($_POST["lname"]))) {
		$editLname = $lname;
		$lname = $error;
		$isok = 0;
	}
	else {
		$editLname = "";
	}
	if(!(isset($_POST["group"]) && $group = validateIsSomethig($_POST["group"]))) {
		$editGroup = $group;
		$group = $error;
	}
	else {
		$editGroup = "";
	}
	if(!(isset($_POST["speciality"]) && $speciality = validateText($_POST["speciality"]))) {
		$editSpeciality = $speciality;
		$speciality = $error;
	}
	else {
		$editSpeciality = "";
	}
	if(isset($_POST["age"]) && $age = validateAge($_POST["age"])) {
		$editAge = $age;
		$age = $age + " ani";
	}
	else {
		$editAge = 0;
		$age = $error;
	}
	if(isset($_POST["sex"]) && $sex = validateSex($_POST["sex"])) {
		$editSex = $sex;
		$sex = $sex=="m"?"Masculin":"Feminin";
	}
	else {
		$editSex = "m";
		$sex = $error;
	}
	if(!(isset($_POST["address"]) && $address = validateIsSomethig($_POST["address"]))) {
		$editAddress = $address;
		$address = $error;
	}
	else {
		$editAddress = "";
	}
	if(!(isset($_POST["username"]) && $username = validateIsSomethig($_POST["username"]))) {
		$editUsername = $username;
		$username = $error;
		$isok = 0;
	}
	else {
		$editUsername = "";
	}
	if(isset($_POST["password"]) && $password = validateIsSomethig($_POST["password"])) {
	}
}
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="UTF-8">
        <title>Magic</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
    </head>

    <body>
        <?php include("include/nav.php"); ?>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Verificare date</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <form action="index.php" method="POST">
                                        <table class="table table-striped table-hover">
                                            <tr>
                                                <th>Nume</th>
                                                <td>
                                                    <?php echo $lname; ?>
                                                        <input type="hidden" name="lname" value="<?php echo $editFname; ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Prenume</th>
                                                <td>
                                                    <?php echo $fname; ?>
                                                        <input type="hidden" name="fname" value="<?php echo $editLname; ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Grupa</th>
                                                <td>
                                                    <?php echo $group; ?>
                                                        <input type="hidden" name="group" value="<?php echo $editGroup; ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Specialitatea</th>
                                                <td>
                                                    <?php echo $speciality; ?>
                                                        <input type="hidden" name="speciality" value="<?php echo $editSpeciality; ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Vârsta</th>
                                                <td>
                                                    <?php echo $age; ?>
                                                        <input type="hidden" name="age" value="<?php echo $editAge; ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Sex</th>
                                                <td>
                                                    <?php echo $sex; ?>
                                                        <input type="hidden" name="sex" value="<?php echo $editSex; ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Adresa</th>
                                                <td>
                                                    <?php echo $address; ?>
                                                        <input type="hidden" name="address" value="<?php echo $editAddress; ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Username</th>
                                                <td>
                                                    <?php echo $username; ?>
                                                        <input type="hidden" name="username" value="<?php echo $editUsername; ?>">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Parola</th>
                                                <td>
                                                    <?php echo "+"; ?>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btns pull-right">
                                            <button type="submit" class="btn btn-default">Modifică</button>
                                            <button type="submit" class="btn btn-primary" formaction="validate.php?write" <?php echo $isok? "": "disabled"; ?>>OK</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="js/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
    </body>

    </html>
