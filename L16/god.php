<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Magic</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
</head>

<body>
    <?php include("include/nav.php"); ?>
    <div class="container">
    	<div class="row">
    		<div class="col-sm-12">
    			<table class="table table-striped table-hover table-bordered">
    				<thead>
    					<tr>
    						<th>Nume</th>
    						<th>Prenume</th>
    						<th>Grupa</th>
    						<th>Specialitatea</th>
    						<th>Vârsta</th>
    						<th>Sex</th>
    						<th>Adresa</th>
    						<th>Username</th>
    						<th>Password</th>
    						<th colspan="2">Acțiuni</th>
    					</tr>
    				</thead>
    				<tbody>
    					<tr>
    						<td>Leahu</td>
    						<td>Igor</td>
    						<td>I-1245</td>
    						<td>Informatica</td>
    						<td>19 ani</td>
    						<td>Masculin</td>
    						<td>str. Unu, ap. Doi</td>
    						<td>iamgod</td>
    						<td>***</td>
    						<td><a href="#"><i class='fa fa-pencil-square'></i></a></td>
    						<td><a href="#"><i class='fa fa-minus-square'></i></a></td>
    					</tr>
    				</tbody>
    			</table>
    		</div>
    	</div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>
