<?php
function generate_combinations(array $data, array $all = array(), array $group = array(), $value = null, $i = 0) {
    $keys = array_keys($data);
    if (isset($value) === true) {
        array_push($group, $value);
    }
    
    if ($i >= count($data)) {
        array_push($all, $group);
    } 
    else {
        $currentKey = $keys[$i];
        $currentElement = $data[$currentKey];
        foreach ($currentElement as $val) {
            generate_combinations($data, $all, $group, $val, $i + 1);
        }
    }
    
    return $all;
}

$data = array(array('a', 'b'), array('e', 'f', 'g'), array('w', 'x', 'y', 'z'),);

$combos = combos($data);
print_r($combos);
?>