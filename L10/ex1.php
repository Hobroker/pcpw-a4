<?php
for ($i = 0; $i < 10; $i++) {
	$q[$i] = rand(5, 40);
}
echo "<br>Tabloul original: ";
foreach ($q as $val) {
    echo "$val ";
}
$sortat = false;
while (false === $sortat) {
    $sortat = true;
    for ($i = 0; $i < count($q) - 1; ++$i) {
        if ($q[$i] > $q[$i + 1]) {
            $temp = $q[$i];
            $q[$i] = $q[$i + 1];
            $q[$i + 1] = $temp;
            $sortat = false;
        }
    }
}

echo "<br>Tabloul sortat crescator: ";
foreach ($q as $val) {
    echo "$val ";
}

$sortat = false;
while (false === $sortat) {
    $sortat = true;
    for ($i = 0; $i < count($q) - 1; ++$i) {
        if ($q[$i] < $q[$i + 1]) {
            $temp = $q[$i];
            $q[$i] = $q[$i + 1];
            $q[$i + 1] = $temp;
            $sortat = false;
        }
    }
}

echo "<br>Tabloul sortat descrescator: ";
foreach ($q as $val) {
    echo "$val ";
}
?>