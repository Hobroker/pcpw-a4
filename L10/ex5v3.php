<?php
function fact($num) {
    if (($num === 1) || ($num === 0)) return 1;
    else return fact($num - 1) * $num;
}
function isdiff($arr, $num) {
    for ($i = 0; $i < count($arr); $i++) if ($num == $arr[$i]) return false;
    return true;
}
function combinari($arr, $n, $m, $mcur) {
    if ($m === $mcur) return $arr;
    elseif ($mcur == 1) {
        $reskey = 0;
        $size = count($arr);
        $res = array();
        for ($i = 0; $i < $n; $i++) {
            for ($j = 0; $j < $size; $j++) if ($arr[$j] != $i) {
                $res[$reskey][0] = $arr[$j];
                $res[$reskey][1] = $i;
                $reskey++;
            }
        }
    } 
    elseif ($mcur < $m) {
        $reskey = 0;
        $size = count($arr);
        $res = array();
        for ($i = 0; $i < $n; $i++) {
            for ($j = 0; $j < $size; $j++) if (isdiff($arr[$j], $i)) $res[$reskey++] = array_merge($arr[$j], array($i));
        }
    }
    return combinari($res, $n, $m, $mcur + 1);
}
function diff_arr($arr1, $arr2) {
    if (count(array_diff($arr1, $arr2)) == 0) return false;
    else return true;
}
function eliminare_dublicate(&$arr) {
    $size = count($arr);
    $res = array();
    $res[0] = $arr[0];
    $reskey = 0;
    for ($i = 1; $i < $size; $i++) {
        $corect = true;
        for ($j = $reskey; $j >= 0; $j--) {
            if (!diff_arr($res[$j], $arr[$i])) {
                $corect = false;
                break;
            }
        }
        if ($corect) {
            $res[++$reskey] = $arr[$i];
        }
    }
    return $res;
}
function combinari_optimizate(&$arr, $n, $m, $mcur, $total = 0) {
    if ($mcur > $m) {
        $total = fact($n) / fact($m) / fact($n - $m);
        if ($mcur == $n) {
            for ($i = 0; $i < $n; $i++) $arr[$i] = range();
        }
        
        $a[0] = range(0, $m);
        $key = 0;
        while ($key != $total - 1) {
            for ($i = 0; $i < $n; $i++) {
                $cache = range(0, $n);
            }
        }
        combinari_optimizate($arr, $n, $m, $mcur - 1, $total);
    } 
    else return true;
}
function clear_value(&$arr, $n) {
    $m = $n - count($arr[0]);
    $full = range(0, $n - 1, 1);
    for ($i = 0; $i < count($arr); $i++) {
        $arr[$i] = array_values(array_diff($full, $arr[$i]));
    }
}
$a = array("Pavel", "Ion", "Vasile", "Dumitru", "Nicolae", "Daniel", "Radu", "Anatol", "Jiku", "Yoda");

//Toti oamenii
echo "<br>----------------------------------------------------------------------------------------------<br>";
$b = range(0, count($a) - 1);
$m = 8;
 //Numarul de oameni in echipa
if ($m == count($b)) {
    $solutia[0] = $b;
} 
else if ($m <= count($a) / 2) {
    $result = combinari($b, count($a), $m, 1);
    $solutia = eliminare_dublicate($result);
} 
else {
    $result = combinari($b, count($a), count($a) - $m, 1);
    $solutia = eliminare_dublicate($result);
    clear_value($solutia, count($a));
}
echo "<br>Combinari= " . count($solutia);
foreach ($solutia as $el) {
    echo "<br>(";
    $sz = count($el);
    for ($i = 0; $i < $sz; $i++) {
        echo $a[$el[$i]];
        if ($i + 1 < $sz) echo ", ";
    }
    echo ")";
}
?>
