<?php
for ($i = 0; $i < 10; $i++) {
    $q[] = rand(5, 40);
}
$q[] = 3;
$sum = 0;
$max = max($q);
$min = min($q);
$maxnr = 0;
$minnr = 0;
foreach ($q as $val) {
    $sum+= $val;
    $maxnr+= $max == $val ? 1 : 0;
    $minnr+= $min == $val ? 1 : 0;
    $prime = true;
    for ($j = 2; $j < $val; $j++) {
        if ($val % $j == 0) {
            $prime = false;
            break;
        }
    }
    if ($prime) $primes[] = $val;
}
echo "<br>Tabloul: ";
foreach ($q as $val) {
    echo "$val ";
}
echo "<br>Suma: $sum";
echo "<br>Maxim: $max, ($maxnr numere)";
echo "<br>Minim: $min, ($minnr numere)";
echo "<br>Exista ".count($primes)." numere prime: ";
foreach ($primes as $val) {
	echo "$val ";
}
?>