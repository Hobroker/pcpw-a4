<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8>
	<title>Tabla inmultirii</title>
	<link rel="stylesheet" href="css/magic.css">
</head>
<body>
<h1>Tabla înmulțirii</h1>
<?php
echo "<table><tr><td class='bold color im' x=1 y=1>✖</td>";
$max = 10;
for ($i = 0; $i <= $max; $i++) {
    echo "<td class='bold color im'>$i</td>";
}
for ($i = 0; $i <= $max; $i++) {
    echo "<tr><td class='bold color im'>$i</td>";
    for ($j = 0; $j <= $max; $j++) {
        echo "<td y=" . ($i + 1) . " x=" . ($j + 1) . ">" . $j * $i . "</td>";
    }
    echo "</tr>";
}
echo "</tr>";
?>
<script src="js/jquery.js"></script>
<script src="js/magic.js"></script>
</body>
</html>