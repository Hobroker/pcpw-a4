$(document).ready(function() {
    $("td:not(.bold)").mouseenter(function() {
        var y = -1,
            row = $(this).attr("y"),
            column = $(this).attr("x");
        $("table tr").each(function() {
            x = -1;
            y++;
            $("td", this).each(function() {
                if (column == ++x && row >= y || row == y && column != x && column >= x) {
                    $(this).addClass('color2')
                } else {
                    $(this).removeClass('color2')
                }
            })
        })
    }).mouseout(function() {
        $("table tr").each(function() {
            $("td", this).each(function() {
                $(this).removeClass('color2')
            })
        })
    })
})