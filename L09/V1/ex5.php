<?php
$a = $_GET['a'];
$b = $_GET['b'];
$c = $_GET['c'];

if (($a + $b > $c) && ($a + $c > $b) && ($b + $c > $a)) {
    echo "Aceste laturi pot forma un triunghi<br/>";
} 
else {
    echo "Lungimile acestor laturi nu pot forma un triunghi<br/>";
}

echo "Triunghiul este: " . (($a == $b && $b == $c) ? "echilateral" : "") . (($a == $b || $b == $c || $c == $a) ? " isoscel" : "") . ((pow($c, 2) == pow($a, 2) + pow($b, 2)) ? " dreptunghi" : "");
$s = $a + $b + $c;
echo "<br>Perimetrul = $s u.c.";
$s/= 2;
echo "<br>Aria = " . round((sqrt($s * ($s - $a) * ($s - $b) * ($s - $c))), 2) . " u.c.";
?>