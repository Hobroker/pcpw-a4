<html>
	<head>
	<link rel="stylesheet" href="style.css">
	</head>
	<body>
<?php
echo "<h1>Grigore Vieru - Templul</h1>";
echo "<pre>Lui Ioan Alexandru
Pretuiesc deopotriva pe sfantul
Cu mana dusa spre inima, 
La fel si pe sfantul sprijinit
In sabie.
Doamne, poate ca
N-am fost nici sfant, nici ostean.
Pur si simplu, ziditu-m-am de viu
In templul Limbii Romane.
Iar in biserica
N-ai cum sa fii curajos.
In biserica sa fii
Drept si cinstit. </pre>";
?>
	</body>
</html>