var qs = [{
        titlu: "Ce zi e azi?",
        rasp: ["Luni", "Marti", "Miercuri", "Vineri"],
        corect: 3
    }, {
        titlu: "Ce pereche e acum?",
        rasp: ["SPG", "SGBD", "PCPW", "POO"],
        corect: 2
    }, {
        titlu: "Ce anotimp e acum?",
        rasp: ["Vara", "Toamna", "Iarna", "Primavara"],
        corect: 3
    }, {
        titlu: "Ce zi e azi?",
        rasp: ["Luni", "Marti", "Miercuri", "Vineri"],
        corect: 3
    }],
    now = 0,
    corect = 0

window.onload = function() {
    nextQ();

    next.onclick = function() {
        var a = questions.children
        for (var i = 0; i < a.length; i++) {
            var b = a[i].firstChild
            if (b.getAttribute("corect") != null && b.checked) {
                corect++;
            }
        }
        if (now <= 3) {
            nextQ();
        } else {
            resutl()
        }
    }
}

function nextQ() {
    queue.innerHTML = (now + 1)
    questions.innerHTML = ""
    titlu.innerHTML = qs[now].titlu
    for (var i = 0; i < qs[now].rasp.length; i++) {
        var a = document.createElement("div")
        var b = document.createElement("input")
        b.type = "radio"
        b.id = "qs" + i
        b.name = "rasp"
        if (i == qs[now].corect) {
            b.setAttribute("corect", "1")
        }
        var c = document.createElement("label")
        c.innerHTML = qs[now].rasp[i]
        c.setAttribute("for", "qs" + i)
        a.appendChild(b)
        a.appendChild(c)
        questions.appendChild(a)
    }
    now++;
}

function resutl() {
    myForm.innerHTML = '<p class="title" id="titlu">Rezultat: ' + corect + ' raspunsuri corecte!</p>'
}
