<?php
$nume1 = "";
$nume = "";
$prenume1 = "";
$prenume = "";
$grupa1 = "";
$grupa = "";
$specialitatea1 = "";
$specialitatea = "";
$adresa1 = "";
$adresa = "";
$varsta1 = "";
$varsta = "";
$sex1 = "";
$sex = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$isok = true;
	include 'global.php';
	if(isset($_POST["nume"]) && strlen($_POST["nume"]) > 0){
	    $nume = $_POST["nume"];
	    $nume1 = $nume;
	}
	else{
	    $nume = $error_icon . $error_s_m;
	    $isok = false;
	}
	if(isset($_POST["prenume"]) && strlen($_POST["prenume"]) > 0){
	    $prenume = $_POST["prenume"];
	    $prenume1 = $prenume;
	}
	else{
	    $prenume = $error_icon . $error_s_m;
	    $isok = false;
	}
	if(isset($_POST["grupa"]) && strlen($_POST["grupa"]) > 0){
	    $grupa = $_POST["grupa"];
	    $grupa1 = $grupa;
	}
	else{
	    $grupa = $error_s_m;
	}
	if(isset($_POST["specialitatea"]) && strlen($_POST["specialitatea"]) > 0){
	    $specialitatea = $_POST["specialitatea"];
	    $specialitatea1 = $specialitatea;
	}
	else{
	    $specialitatea = $error_s_f;
	}
	if(isset($_POST["adresa"]) && strlen($_POST["adresa"]) > 0){
	    $adresa = $_POST["adresa"];
	    $adresa1 = $adresa;
	}
	else{
	    $adresa = $error_s_f;
	}
	if(isset($_POST["varsta"]) && is_numeric($_POST["varsta"]) && $_POST["varsta"] > 0 && $_POST["varsta"] < 101){
	    $varsta1 = $_POST["varsta"];
	    $varsta = $varsta1 . " ani";
	}
	else{
	    $varsta = $error_s_f;
	}
	if(isset($_POST["sex"]) && in_array($_POST["sex"], array("m", "f", "n"))) {
	    $sex = sex($_POST["sex"]);
	    $sex1 = $_POST["sex"];
	}
	else {
	    $sex = $error_s_m;
	}
	if(isset($_GET["write"]) && $isok) {
		include 'connect.php';
	    $magic = $con->prepare('INSERT INTO elev (nume, prenume, grupa, specialitatea, adresa, varsta, sex) VALUES (?, ?, ?, ?, ?, ?, ?)');
	    $magic->bind_param("sssssss",$nume, $prenume, $grupa, $specialitatea, $adresa, $varsta, $sex);

	    $nume = $_POST["nume"];
	    $prenume = $_POST["prenume"];
	    $grupa = $_POST["grupa"];
	    $specialitatea = $_POST["specialitatea"];
	    $adresa = $_POST["adresa"];
	    $varsta = $_POST["varsta"];
	    $sex = $_POST["sex"];

	    $magic->execute();
	    header('Location: all.php');
	}
	elseif(isset($_GET["update"])) {
		include 'connect.php';
	    $magic = $con->prepare('UPDATE elev SET nume = ?, prenume = ?, grupa = ?, specialitatea = ?, adresa = ?, varsta = ?, sex = ? WHERE ID = ?');
	    $magic->bind_param("ssssssss",$nume, $prenume, $grupa, $specialitatea, $adresa, $varsta, $sex, $id);

	    $nume = $_POST["nume"];
	    $prenume = $_POST["prenume"];
	    $grupa = $_POST["grupa"];
	    $specialitatea = $_POST["specialitatea"];
	    $adresa = $_POST["adresa"];
	    $varsta = $_POST["varsta"];
	    $sex = $_POST["sex"];
	    $id = $_GET["update"];

	    $magic->execute();
	    header('Location: all.php');
	}
}
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="UTF-8">
        <title>Verificare Date</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/mag.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
    </head>

    <body class="bg-flat">
        <?php include '/include/nav.php'; ?>
            <div class="container">
                <form class="form table" action="index.php" method="POST">
                    <h1>Verificare date</h1>
                    <table>
                        <tr>
                            <td>Nume</td>
                            <td>
                                <input type="hidden" name="nume" value="<?php echo $nume1; ?>">
                                <?php echo $nume; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Prenume</td>
                            <td>
                                <input type="hidden" name="prenume" value="<?php echo $prenume1; ?>">
                                <?php echo $prenume; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Grupa</td>
                            <td>
                                <input type="hidden" name="grupa" value="<?php echo $grupa1; ?>">
                                <?php echo $grupa; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Specialitatea</td>
                            <td>
                                <input type="hidden" name="specialitatea" value="<?php echo $specialitatea1; ?>">
                                <?php echo $specialitatea; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Vârsta</td>
                            <td>
                                <input type="hidden" name="varsta" value="<?php echo $varsta1; ?>">
                                <?php echo $varsta; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Sex</td>
                            <td>
                                <input type="hidden" name="sex" value="<?php echo $sex1; ?>">
                                <?php echo $sex; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Adresa</td>
                            <td>
                                <input type="hidden" name="adresa" value="<?php echo $adresa1; ?>">
                                <?php echo $adresa; ?>
                            </td>
                        </tr>
                    </table>
                    <div class="form-fieldset form-footer">
                        <div class="form-group">
                            <input type="submit" value="OK" class="btn btn-first" formaction="validate.php?write" name="add" <?php echo $isok?"":"disabled"; ?>>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Modifică" class="btn btn-second" name="edit">
                        </div>
                    </div>
                </form>
            </div>
    </body>

    </html>
