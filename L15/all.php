<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Toate datele</title>
    <link rel="stylesheet" href="css/mag.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
</head>

<body class="bg-flat">
    <?php include '/include/nav.php'; ?>
        <div class="container large">
            <table class="single">
                <thead>
                    <tr>
                        <th><a class="<?php echo isset($_GET["sort"])&&$_GET["sort"]=="nume"?"under":""; ?>" href="?sort=nume">Nume</a></th>
                        <th><a class="<?php echo isset($_GET["sort"])&&$_GET["sort"]=="prenume"?"under":""; ?>" href="?sort=prenume">Prenume</a></th>
                        <th><a class="<?php echo isset($_GET["sort"])&&$_GET["sort"]=="grupa"?"under":""; ?>" href="?sort=grupa">Grupa</a></th>
                        <th><a class="<?php echo isset($_GET["sort"])&&$_GET["sort"]=="specialitatea"?"under":""; ?>" href="?sort=specialitatea">Specialitatea</a></th>
                        <th><a class="<?php echo isset($_GET["sort"])&&$_GET["sort"]=="varsta"?"under":""; ?>" href="?sort=varsta">Vârsta</a></th>
                        <th><a class="<?php echo isset($_GET["sort"])&&$_GET["sort"]=="sex"?"under":""; ?>" href="?sort=sex">Sex</a></th>
                        <th><a class="<?php echo isset($_GET["sort"])&&$_GET["sort"]=="adresa"?"under":""; ?>" href="?sort=adresa">Adresa</a></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
include 'connect.php';
include 'global.php';
$key = isset($_GET["q"])?"%" . $_GET["q"] . "%":"%";
$q = $con->prepare('SELECT * FROM elev WHERE nume LIKE ? OR prenume LIKE ? OR grupa LIKE ? OR specialitatea LIKE ? OR varsta LIKE ? OR sex LIKE ? OR adresa LIKE ? ORDER BY ' . (isset($_GET["sort"])?$_GET["sort"]:"ID"));
$q->bind_param("sssssss", $key, $key, $key, $key, $key, $key, $key);
$q->execute();
$data = $q->get_result();
while($row = $data->fetch_assoc()) {
    echo "<tr>";
    echo "<td>" . $row["nume"] . "</td>";
    echo "<td>" . $row["prenume"] . "</td>";
    echo "<td>" . $row["grupa"] . "</td>";
    echo "<td>" . $row["specialitatea"] . "</td>";
    echo "<td>" . $row["varsta"] . "</td>";
    echo "<td>" . sex($row["sex"]) . "</td>";
    echo "<td>" . $row["adresa"] . "</td>";
    echo "<td class='options a'><a title='Șterge' href='global.php?deleteelev=" . $row["ID"] . "'><i class='fa fa-minus-square'></i></a></td>";
    echo "<td class='options'><a title='Modifică' href='index.php?editelev=" . $row["ID"] . "'><i class='fa fa-pencil-square'></i></a></td>";
    echo "</tr>";
}
?>
                </tbody>
            </table>
        </div>
</body>

</html>
