<?php
$nume = "";
$prenume = "";
$grupa = "";
$specialitatea = "";
$varsta = "";
$sex = "n";
$adresa = "";
if(isset($_GET['editelev'])) {
    include 'connect.php';
    $q = $con->prepare('SELECT * FROM elev WHERE ID=?');
    $q->bind_param("s", $id);
    $id = $_GET['editelev'];
    $q->execute();
    $data = $q->get_result();
    $row = $data->fetch_assoc();
    $nume = $row['nume'];
    $prenume = $row['prenume'];
    $grupa = $row['grupa'];
    $specialitatea = $row['specialitatea'];
    $varsta = $row['varsta'];
    $sex = $row['sex'];
    $adresa = $row['adresa'];
}
else {
    include 'global.php';
    if(isset($_POST["nume"]) && valid($_POST["nume"])){
        $nume = $_POST["nume"];
    }
    if(isset($_POST["prenume"]) && valid($_POST["prenume"])){
        $prenume = $_POST["prenume"];
    }
    if(isset($_POST["grupa"]) && valid($_POST["grupa"])){
        $grupa = $_POST["grupa"];
    }
    if(isset($_POST["specialitatea"]) && valid($_POST["specialitatea"])){
        $specialitatea = $_POST["specialitatea"];
    }
    if(isset($_POST["adresa"]) && valid($_POST["adresa"])){
        $adresa = $_POST["adresa"];
    }
    if(isset($_POST["varsta"]) && is_numeric($_POST["varsta"]) && $_POST["varsta"] > 0 && $_POST["varsta"] < 101){
        $varsta = $_POST["varsta"];
    }
    if(isset($_POST["sex"]) && in_array($_POST["sex"], array("m", "f", "n"))) {
        $sex = $_POST["sex"];
    }
}
?>
    <!DOCTYPE html>
    <html>

    <head>
        <title>Formular</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/mag.css">
    </head>

    <body class="bg-flat">
        <?php include '/include/nav.php'; ?>
            <div class="container tabs">
                <div class="tabs">
                    <input type="radio" name="tabs" id="tab1" checked>
                    <label for="tab1" class="tab-header">
                        Înregistare
                    </label>
                    <input type="radio" name="tabs" id="tab2" <?php echo isset($_GET["signin"])?"checked":""; ?>>
<!--                     <label for="tab2" class="tab-header">
                        Logare
                    </label> -->
                    <div id="tab-content1" class="tab-content">
                        <form class="form inputs" action="validate.php<?php echo isset($_GET['editelev'])?" ?update=".$_GET['editelev']:" "; ?>" method="POST">
                            <div class="form-fieldset col-2">
                                <div class="form-group">
                                    <label class="input">
                                        <input placeholder="Nume" type="text" name="nume" value="<?php echo $nume; ?>">
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="input">
                                        <input placeholder="Prenume" type="text" name="prenume" value="<?php echo $prenume; ?>">
                                    </label>
                                </div>
                            </div>
                            <div class="form-fieldset col-2">
                                <div class="form-group">
                                    <label class="input">
                                        <input placeholder="Grupa" type="text" name="grupa" value="<?php echo $grupa; ?>">
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="input">
                                        <input placeholder="Specialitatea" type="text" name="specialitatea" value="<?php echo $specialitatea; ?>">
                                    </label>
                                </div>
                            </div>
                            <div class="form-fieldset col-2">
                                <div class="form-group">
                                    <label class="input">
                                        <input placeholder="Vârsta" type="text" name="varsta" value="<?php echo $varsta; ?>">
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="select">
                                        <select name="sex">
                                            <option <?php echo $sex=='m' ? 'selected="true" ': ''; ?>value="m">Masculin</option>
                                            <option <?php echo $sex=='f' ? 'selected="true" ': ''; ?>value="f">Feminin</option>
                                            <option <?php echo $sex=='n' ? 'selected="true" ': ''; ?>value="n">Alt</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                            <div class="form-fieldset">
                                <div class="form-group">
                                    <label class="input">
                                        <input placeholder="Adresa" type="text" name="adresa" value="<?php echo $adresa; ?>">
                                    </label>
                                </div>
                            </div>
                            <div class="form-fieldset form-footer">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class="btn btn-first">
                                </div>
                                <div class="form-group">
                                    <input type="reset" value="Reset" class="btn btn-second">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="tab-content2" class="tab-content">
                        <form class="form inputs" action="validate.php<?php echo isset($_GET['editelev'])?" ?update=".$_GET['editelev']:" "; ?>" method="POST">
                            <div class="form-fieldset">
                                <div class="form-group">
                                    <label class="input">
                                        <input placeholder="Nume" type="text" name="nume" value="<?php echo $nume; ?>">
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="input">
                                        <input placeholder="Prenume" type="text" name="prenume" value="<?php echo $prenume; ?>">
                                    </label>
                                </div>
                            </div>
                            <div class="form-fieldset form-footer">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class="btn btn-first">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </body>

    </html>
