<?php
include("global.php");
$fname = "";
$lname = "";
$email = "";
$age = "";
$d = -1;
$m = -1;
$y = -1;
$sex = "";
$limbi = array();
if (isset($_POST['edit']) &&  isset($_POST["fname"]) && isset($_POST["lname"]) && isset($_POST["email"]) && isset($_POST["age"]) && isset($_POST["d"]) && isset($_POST["m"]) && isset($_POST["y"]) && isset($_POST["sex"])) {
    $fname = $_POST["fname"];
    $lname = $_POST["lname"];
    $email = $_POST["email"];
    if(is_numeric($_POST["age"]) && $_POST["age"] > 0 && $_POST["age"] < 101) {
        $age = $_POST["age"];
    }
    else {
        $age = "";
    }
    if(is_numeric($_POST["d"]) && is_numeric($_POST["m"]) && is_numeric($_POST["y"])) {
        $d = $_POST["d"];
        $m = $_POST["m"];
        $y = $_POST["y"];
    }
    else {
        $d = "";
        $m = "";
        $y = "";
    }
    if(in_array($_POST["sex"], array("m", "f", "n"))) {
        $sex = $_POST["sex"];
    }
    else {
        $sex = "";
    }
    $limbi = explode(",", $_POST["limbi"]);
}
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="UTF-8">
        <title>Formular</title>
        <link rel="stylesheet" href="css/mag.css">
    </head>

    <body>
        <div class="container">
            <form method="POST" action="end.php" class="f">
                <div class="f-control">
                    <h1>Formular</h1>
                </div>
                <div class="f-control half">
                    <label for="fname">Prenume</label>
                    <input id="fname" type="text" name="fname" value="<?php echo $fname; ?>">
                </div>
                <div class="f-control half">
                    <label for="lname">Nume</label>
                    <input id="lname" type="text" name="lname" value="<?php echo $lname; ?>">
                </div>
                <div class="f-control half">
                    <label for="age">Varsta</label>
                    <input id="age" type="number" name="age" min="1" max="100" value="<?php echo $age; ?>">
                </div>
                <div class="f-control half">
                    <label>Sex</label>
                    <select name="sex">
                        <option <?php echo $sex=="n" ? "selected": ""; ?> value="n">Altul</option>
                        <option <?php echo $sex=="m" ? "selected": ""; ?> value="m">Masculin</option>
                        <option <?php echo $sex=="f" ? "selected": ""; ?> value="f">Feminin</option>
                    </select>
                </div>
                <div class="f-control f-3">
                    <label>Data nașterii</label>
                    <select name="d">
                        <option value="-1" selected>Ziua</option>
                        <?php
                    for($i=1; $i<=31; $i++) {
                        echo "<option " . (($i==$d)?"selected":"") . " value='" . $i . "'>" . $i . "</option>";
                    }
                ?>
                    </select>
                    <select name="m">
                        <option value="-1" selected>Luna</option>
                        <?php
                    for($i=0; $i<count($luni); $i++) {
                        echo "<option " . (($i==$m)?"selected":"") . " value='" . $i . "'>" . $luni[$i] . "</option>";
                    }
                ?>
                    </select>
                    <select name="y">
                        <option value="-1" selected>Anul</option>
                        <?php
                    for($i=2015; $i>=1900; $i--) {
                        echo "<option " . (($i==$y)?"selected":"") . " value='" . $i . "'>" . $i . "</option>";
                    }
                ?>
                    </select>
                </div>
                <div class="f-control">
                    <label for="email">Email</label>
                    <input id="email" type="email" name="email" value="<?php echo $email; ?>">
                </div>
                <div class="f-control">
                    <label>Limbi cunoscute</label>
                    <table>
                        <?php
                        foreach ($langs as $key => $value) {
                            echo "<tr>";
                            echo "<td><input type='checkbox' name='" . $key . "' id='" . $key . "' " . (in_array( $key, $limbi)?'checked':'') . "></td>";
                            echo "<td><label for='" . $key . "' class='sub'>".$value."</label></td>";
                            echo "</tr>";
                        }
                    ?>
                    </table>
                </div>
                <div class="f-control inline i2">
                    <input class="btn default" type="reset" value="Reset">
                </div>
                <div class="f-control inline i1">
                    <input class="btn primary" type="submit" value="Submit">
                </div>
            </form>
        </div>
    </body>

    </html>
