<?php
$langs = array("en" => "Engleză",
			   "ro" => "Romană",
			   "ru" => "Rusă",
			   "fr" => "Franceză",
			   "de" => "Germană");
$luni = array('Ianuarie', 'Februarie', 'Martie', 'Aprilie', 'Mai', 'Iunie', 'Iulie', 'August', 'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie');
function between($val, $q, $w) {
    return ($val >= $q && $val <= $w) || ($val <= $q && $val >= $w);
}
?>
