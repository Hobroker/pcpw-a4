<?php
include("global.php");
if(isset($_POST["fname"]) && strlen($_POST["fname"]) > 0){
    $fname = $_POST["fname"];
    $fname1 = $fname;
}
else{
    $fname = "<span class='error'>Nu este specificat</span>";
    $fname1 = "";
}
if(isset($_POST["lname"]) && strlen($_POST["lname"]) > 0){
    $lname = $_POST["lname"];
    $lname1 = $lname;
}
else{
    $lname = "<span class='error'>Nu este specificat</span>";
    $lname1 = "";
}
if(isset($_POST["age"]) && is_numeric($_POST["age"]) && $_POST["age"] > 0 && $_POST["age"] < 101){
    $age = $_POST["age"] + "ani";
    $age1 = $age;
}
else{
    $age = "<span class='error'>Nu este specificată</span>";
    $age1 = "";
}
if(isset($_POST["email"]) && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
    $email = $_POST["email"];
    $email1 = $email;
}
else{
    $email = "<span class='error'>Nu este specificat</span>";
    $email1 = "";
}
if(isset($_POST["sex"]) && in_array($_POST["sex"], array("m", "f", "n"))) {
    if(($_POST["sex"]=="m")) {
        $sex = "Masculin";
    }
    elseif (($_POST["sex"]=="f")) {
        $sex = "Feminin";
    }
    else {
        $sex = "Altul";
    }
    $sex1 = $_POST["sex"];
}
else {
    $sex = "<span class='error'>Nu este specificat</span>";
    $sex1 = "";
}
if(isset($_POST["d"]) && isset($_POST["m"]) && isset($_POST["y"]) && is_numeric($_POST["d"]) && is_numeric($_POST["m"]) && is_numeric($_POST["y"]) && checkdate($_POST["m"], $_POST["d"], $_POST["y"])) {
    $date = $_POST["d"] . " " . $luni[$_POST["m"]] . " " . $_POST["y"];
    $d = $_POST["d"];
    $m = $_POST["m"];
    $y = $_POST["y"];
} else {
    $date = "<span class='error'>Nu este specificată</span>";
    $d = isset($_POST["d"])?$_POST["d"]:"";
    $m = isset($_POST["m"])?$_POST["m"]:"";
    $y = isset($_POST["y"])?$_POST["y"]:"";
}
$err_limbi = true;
$limbi = "";
$limbi1 = "";
foreach ($langs as $key => $value) {
    if(isset($_POST[$key])) {
        $err_limbi = false;
        $limbi .= $value.", ";
        $limbi1 .= $key.",";
        $err_limbi = false;
    }
}
if(!$err_limbi) {
    $limbi = substr($limbi, 0, -2);
    $limbi1 = substr($limbi1, 0, -1);
}
else {
    $limbi1 = "";
    $limbi = "<span class='error'>Nu au fost specificate</span>";
}
?>
    <html>

    <head>
        <meta charset="UTF-8">
        <title>Formular</title>
        <link rel="stylesheet" href="css/mag.css">
    </head>

    <body>
        <div class="container result">
            <div>
                <h1>Datele</h1>
                <form action="index.php" method="POST">
                    <table>
                        <tr>
                            <td>Prenume</td>
                            <td>
                                <?php echo $fname; ?>
                                    <input type="hidden" name="fname" value="<?php echo $fname1; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Nume</td>
                            <td>
                                <?php echo $lname; ?>
                                    <input type="hidden" name="lname" value="<?php echo $lname1; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Vârsta</td>
                            <td>
                                <?php echo $age; ?>
                                    <input type="hidden" name="age" value="<?php echo $age1; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Data nașterii</td>
                            <td>
                                <?php echo $date; ?>
                                    <input type="hidden" name="d" value="<?php echo $d; ?>">
                                    <input type="hidden" name="m" value="<?php echo $m; ?>">
                                    <input type="hidden" name="y" value="<?php echo $y; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Sex</td>
                            <td>
                                <?php echo $sex; ?>
                                    <input type="hidden" name="sex" value="<?php echo $sex1; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>
                                <?php echo $email; ?>
                                    <input type="hidden" name="email" value="<?php echo $email1; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>Limbi cunoscute</td>
                            <td>
                                <?php echo $limbi; ?>
                                    <input type="hidden" name="limbi" value="<?php echo $limbi1; ?>">
                            </td>
                        </tr>
                    </table>
                    <div class="f-control inline half">
                        <input class="btn default" type="submit" value="Modifică" name="edit">
                    </div>
                    <div class="f-control inline half">
                        <input class="btn primary" type="button" value="OK">
                    </div>
                </form>
            </div>
        </div>
    </body>

    </html>
