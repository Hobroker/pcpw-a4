<?php
$n = $_GET["n"];
$m = $_GET["m"];
echo "P<sub>n</sub> ($n) = " . perm($n);
echo "<br><br>A<sub>n</sub><sup>m</sup> ($n) = " . aran($n, $m);
echo "<br><br>C<sub>n</sub><sup>m</sup> ($n) = " . comb($n, $m);

function perm($n) {
    $q = 1;
    for ($i = 1; $i <= $n; $i++) {
        $q*= $i;
    }
    return $q;
}
function aran($n, $m) {
    return perm($n) / perm($n - $m);
}
function comb($n, $m) {
    return (perm($n) / ((perm($m) * perm($n - $m))));
}
?>