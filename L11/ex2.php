<?php
$n = $_GET["n"];
echo '$n = ' . $n;
sumperm($n);

function perm($n) {
    $q = 1;
    for ($i = 1; $i <= $n; $i++) {
        $q*= $i;
    }
    return $q;
}

function sumperm($n) {
	$sum = 0;
	for($i=1;$i<=$n;$i++) {
		echo "$i! " . ($i==$n?"= ":"+ ");
		$sum+=perm($i);
	}
	echo "$sum";
}
?>